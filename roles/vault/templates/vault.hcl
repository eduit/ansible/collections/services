log_level = "debug"
ui = true
disable_mlock = true
cluster_addr  = "https://{{ inventory_hostname_short }}.ethz.ch:8201"
api_addr      = "https://{{ inventory_hostname_short }}.ethz.ch:8200"

storage "raft" {
  path = "{{ vault_root }}"
  {% if vault_type == "backend" %}
  {% for host in vault_peers %}
  retry_join {
    leader_api_addr = "https://{{ host }}.ethz.ch:8200"
  }
  {% endfor %}
  {% endif %}
}

listener "tcp" {
  address       = "0.0.0.0:8200"
  tls_cert_file = "/opt/vault/tls/tls.crt"
  tls_key_file  = "/opt/vault/tls/tls.key"
}

{% if vault_type == "backend" %}
seal "transit" {
  address = "https://{{ vault_frontends[0] }}.ethz.ch:8200"
  disable_renewal = "false"
  key_name = "autounseal"
  mount_path = "transit/"
  # tls_skip_verify = "true"
}
{% endif %}
