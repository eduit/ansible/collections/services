# Role let.services.matomo

## group_vars/Matomo.yml
- `selinux_disable`: true
- `mariadb_db_password`: password (encryped) for DB user matomo
- `mariadb_users`: see below
- `mariadb_databses`: see below

## host_vars/<hostname.<ml>
- `matomo_hostname`: main address, ie. matomo.ethz.ch
- `acme_sh_subject_names`: list with hostnames for TLS certificate
- `acmedns_username`, `acmedns_password`, `acmedns_subdomain`: values from Netcenter

## Database Setup
Preconfigured in `group_vars/Matomo.yml`:
```
mariadb_users:
  - name: matomo
    host: "127.0.0.1"
    password: "{{ mariadb_db_password }}"
    priv: "piwik.*:ALL"
    state: present

mariadb_databases:
  - name: piwik
    collation: utf8mb4_general_ci
    encoding: utf8mb4
    replicate: false
```
