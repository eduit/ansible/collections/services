---
- name: Ensure old Matomo ZIP is absent
  ansible.builtin.file:
    path: /root/matomo.zip
    state: absent

- name: Download Matomo ZIP
  ansible.builtin.get_url:
    url: https://builds.matomo.org/matomo.zip
    dest: /root/matomo.zip
    owner: root
    group: root
    mode: "0644"

- name: Ensure temporary directory for installation exists
  ansible.builtin.file:
    path: /var/www/html/update
    state: directory
    owner: apache
    group: apache
    mode: "0755"

- name: Remove existing update directory
  ansible.builtin.file:
    path: "{{ item }}"
    state: absent
  loop:
    - /var/www/html/update/matomo
    - /var/www/html/update/matomo_old

- name: Extract archive
  ansible.builtin.unarchive:
    src: /root/matomo.zip
    remote_src: true
    dest: /var/www/html/update

- name: Make matomo directory writeable
  ansible.builtin.file:
    path: /var/www/html/update/matomo
    state: directory
    recurse: true
    owner: apache

- name: Copy PHP configuration from active environment
  ansible.builtin.copy:
    src: /var/www/html/matomo/config/config.ini.php
    remote_src: true
    dest: /var/www/html/update/matomo/config/config.ini.php
    owner: apache
    group: apache
    mode: "0644"

- name: Copy GeoIP DB from active environment
  ansible.builtin.copy:
    src: /var/www/html/matomo/misc/DBIP-City.mmdb
    remote_src: true
    dest: /var/www/html/update/matomo/misc/DBIP-City.mmdb
    owner: apache
    group: apache
    mode: "0644"

# Update process start
- name: Enable maintenance mode
  community.general.ini_file:
    path: "{{ item }}"
    section: General
    option: maintenance_mode
    value: 1
    owner: apache
    group: apache
    mode: "0644"
  loop:
    - /var/www/html/matomo/config/config.ini.php
    - /var/www/html/update/matomo/config/config.ini.php

- name: Create SQL dump
  ansible.builtin.shell:
    cmd: "mysqldump --add-drop-table matomo > /data_1/{{ ansible_date_time.date }}_matomo.sql"
    creates: "/data_1/{{ ansible_date_time.date }}_matomo.sql"

- name: Swap matomo directories
  ansible.builtin.shell:
    chdir: /var/www/html
    cmd: "mv matomo matomo_old && mv update/matomo . && mv matomo_old update"
    creates: /var/www/html/update/matomo_old

- name: Perform update
  ansible.builtin.command:
    cmd: "/opt/remi/{{ matomo_php_version }}/root/bin/php /var/www/html/matomo/console core:update"
  become: true
  become_user: apache
  changed_when: false

- name: Disable maintenance mode
  community.general.ini_file:
    path: /var/www/html/matomo/config/config.ini.php
    section: General
    option: maintenance_mode
    value: 0
    owner: apache
    group: apache
    mode: "0644"
