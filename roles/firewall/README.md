# Firewall Settings
We currently keep all service related firewall settings in this role. Might be easier to copy/paste, and some settings can be shared amongst services.

## Structure
The role cannot be applied by itself. Instead it provides task lists for different services.

## Usage
For a service, add a task like this one:

```
- name: Apply MariaDB firewall rules
  ansible.builtin.include_role:
    name: let.services.firewall
    tasks_from: mariadb
```
The `tasks_from` selects a particular task list, and only this task list.

## Settings
### Webserver
Adds service definitions for *http* and *https* to the firewall.

### Tftpserver
Opens port 69 to TCP and UDP traffic.

### Mariadb
Currently, only traffic from Kubernetes is supported. WIP.

Opens the *firewalld* standard service *mysql* for traffic from `k8s_ip_range`. This variable is defined in the `group_hosts` settings in [linux_tasks](https://gitlab.ethz.ch/eduit/ansible/playbooks/linux-tasks) for

- Development, Testing: subnet of clusters let-03 and let-04
- Staging, Production: subnet of clusters let-03 and let-04
- Exam: TBD, upcoming subnet of k8s exam cluster
