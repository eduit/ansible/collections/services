<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

@ini_set('session.lazy_write', 'Off');
$CFG->sslproxy  = true;

$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = '{{ moodle_db_host }}';
$CFG->dbname    = '{{ moodle_db_name }}';
$CFG->dbuser    = '{{ moodle_db_user }}';
$CFG->dbpass    = '{{ moodle_db_password }}';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => 5432,
  'dbsocket' => '',
);

$CFG->wwwroot   = '{{ moodle_url }}';
$CFG->dataroot  = '{{ moodle_data_root }}';
$CFG->dirroot   = '{{ moodle_doc_root }}';
$CFG->admin     = 'admin';

$CFG->directorypermissions = 0777;
{% if groups.Develompent is defined %}
$CFG->preventexecpath = true;
@error_reporting(0);
{% endif %}

{% if groups.Development is not defined %}
$CFG->passwordsaltmain = '{&~wD.rDQ^E<<-Kz&{&Ip}{;{2*lz1ic';
$CFG->defaultblocks_topics = ':';
$CFG->disableonclickaddoninstall = true;
$CFG->alternateloginurl = '{{ moodle_url }}/auth/shibboleth/login.php';
$CFG->directorypermissions = 0777;
$CFG->localcachedir = "{{ moodle_cache_dir }}";
$CFG->tempdir = "{{ moodle_temp_dir }}";

//Redis Cache
$CFG->session_handler_class = '\core\session\redis';
$CFG->session_redis_host = '{{ moodle_redis_host }}';
$CFG->session_redis_port = 6379;
$CFG->session_redis_database = 0;
$CFG->session_redis_prefix = 'red.sess.';
$CFG->session_redis_acquire_lock_timeout = 120;
$CFG->session_redis_lock_expire = 7200;
{% endif %}

{%if groups.Production is defined %}
define('MAX_MODINFO_CACHE_SIZE', 2000);
define('CONTEXT_CACHE_MAX_SIZE', 80000);
{% endif %}

require_once(__DIR__ . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
