let.services.moodle
=========

Moodle setup for EduIT

Role Variables
--------------

### Server Roles
Hosts in _Development_ get all necessary roles, for a single server setup. TSM backup is disabled by default,
set Ansible option _backup_ in SnipeIT.

Cluster need a definition in `cluster_vars/<SnipeIT ClusterID>.yml`:
```yaml
moodle_frontend:
    dbr_hosts: 
	  - frontend-00
	  - frontend-01
    cronjob_host: frontend-01
    coursebackup_host: frontend-04
moodle_backend:
    redis_host: backend-01
    nfs_host: backend-01
    db_host: backend-02
    db_log_host: backend-03
    ollama_host: backend-04
```

Execution Order
---------------
**Important**: the host acting as NFS server must be completed first in order to allow the other hosts
to mount the shared volumes. The NFS will reboot if the firewall configuration for NFS has been added.
This is required.


License
-------

BSD
