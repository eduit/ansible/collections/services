$CFG->session_handler_class = '\core\session\redis';
$CFG->session_redis_host = '127.0.0.1';
$CFG->session_redis_port = 6379;  // Optional.
#$CFG->session_redis_auth = '5XpLkMpoMOVYlsI3Q/fheFpzf3Fy3Dp3fE7bmhHEE7fSjVCeX8uk1y7V92gYXej0mJRaM+aWpex94UAO';  // Optional, default is don't set one.
$CFG->session_redis_database = 0;  // Optional, default is db 0.
$CFG->session_redis_prefix = 'red.sess.'; // Optional, default is don't set one.
$CFG->session_redis_acquire_lock_timeout = 120;
$CFG->session_redis_lock_expire = 7200;
