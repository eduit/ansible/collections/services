#!/bin/bash
#
# PostgreSQL Backup Script
# Version 1.0
# Copyright (c) NET, ETH ZURICH, info@net.ethz.ch
#
# This program is based on the following MySQL Backup Script:
# VER. 2.5 - http://sourceforge.net/projects/automysqlbackup/
# Copyright (c) 2002-2003 wipe_out@lycos.co.uk
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#=====================================================================
#=====================================================================
# Set the following variables to your system needs
# (Detailed instructions below variables)
#=====================================================================

# List of DBNAMES for Daily/Weekly Backup e.g. "DB1 DB2 DB3"
# (PostgreSQL databases can be listed using the following command: 'sudo -u postgres psql -l')
DBNAMES="op_portal_prd_01"

# Backup directory location e.g /backups
BACKUPDIR="/var/lib/pgsql/14/backups/"

# Mail setup
# What would you like to be mailed to you?
# - log   : send only log file
# - files : send log file and sql files as attachments (see docs)
# - stdout : will simply output the log to the screen if run manually.
# - quiet : Only send logs if an error occurs to the MAILADDR.
MAILCONTENT="quiet"

# Set the maximum allowed email size in k. (4000 = approx 5MB email [see docs])
MAXATTSIZE="4000"

# Email Address to send mail to? (user@domain.com)
MAILADDR="serveradmin@let.ethz.ch"


# ============================================================
# === ADVANCED OPTIONS ( Read the doc's below for details )===
#=============================================================

# List of DBBNAMES for Monthly Backups.
MDBNAMES="$DBNAMES"

# List of DBNAMES to EXLUCDE if DBNAMES are set to all (must be in " quotes)
# (PostgreSQL databases can be listed using the following command: 'sudo -u postgres psql -l')
DBEXCLUDE="template0 template1"

# Include DROP DATABASE in backup?
DROP_DATABASE=yes

# Separate backup directory and file for each DB? (yes or no)
SEPDIR=yes

# Which day do you want weekly backups? (1 to 7 where 1 is Monday)
DOWEEKLY=7

# Choose Compression type. (gzip or bzip2)
COMP=gzip

# Additionally keep a copy of the most recent backup in a seperate directory.
LATEST=no

# Command to run before backups (uncomment to use)
#PREBACKUP="/etc/postgresql-backup-pre"

# Command run after backups (uncomment to use)
#POSTBACKUP="/etc/postgresql-backup-post"

#=====================================================================
# Options documantation
#=====================================================================
#
# Put in the list of DBNAMES(Databases)to be backed up. If you would like
# to backup ALL DBs on the server set DBNAMES="all".(if set to "all" then
# any new DBs will automatically be backed up without needing to modify
# this backup script when a new DB is created).
#
# If the DB you want to backup has a space in the name replace the space
# with a % e.g. "data base" will become "data%base"
# NOTE: Spaces in DB names may not work correctly when SEPDIR=no.
#
# You can change the backup storage location from /backups to anything
# you like by using the BACKUPDIR setting..
#
# The MAILCONTENT and MAILADDR options and pretty self explanitory, use
# these to have the backup log mailed to you at any email address or multiple
# email addresses in a space seperated list.
# (If you set mail content to "log" you will require access to the "mail" program
# on your server. If you set this to "files" you will have to have mutt installed
# on your server. If you set it to "stdout" it will log to the screen if run from 
# the console or to the cron job owner if run through cron. If you set it to "quiet"
# logs will only be mailed if there are errors reported. )
#
# MAXATTSIZE sets the largest allowed email attachments total (all backup files) you
# want the script to send. This is the size before it is encoded to be sent as an email
# so if your mail server will allow a maximum mail size of 5MB I would suggest setting
# MAXATTSIZE to be 25% smaller than that so a setting of 4000 would probably be fine.
#
# Finally copy automysqlbackup.sh to anywhere on your server and make sure
# to set executable permission. You can also copy the script to
# /etc/cron.daily to have it execute automatically every night or simply
# place a symlink in /etc/cron.daily to the file if you wish to keep it 
# somwhere else.
# NOTE:On Debian copy the file with no extention for it to be run
# by cron e.g just name the file "automysqlbackup"
#
# Thats it..
#
#
# === Advanced options doc's ===
#
# The list of MDBNAMES is the DB's to be backed up only monthly. 
# NOTE: If DBNAMES="all" then MDBNAMES has no effect as all DBs will be backed
# up anyway.
#
# If you set DBNAMES="all" you can configure the option DBEXCLUDE. Other
# wise this option will not be used.
# This option can be used if you want to backup all dbs, but you want 
# exclude some of them. (eg. a db is to big).
#
# Set DROP_DATABASE to "yes" (the default) if you want your SQL-Dump to prior drop
# the databases before creating them again when restoring.
# NOTE: Not used if SEPDIR=no
#
# The SEPDIR option allows you to choose to have all DBs backed up to
# a single file (fast restore of entire server in case of crash) or to
# seperate directories for each DB (each DB can be restored seperately
# in case of single DB corruption or loss).
#
# To set the day of the week that you would like the weekly backup to happen
# set the DOWEEKLY setting, this can be a value from 1 to 7 where 1 is Monday,
# The default is 6 which means that weekly backups are done on a Saturday.
#
# COMP is used to choose the copmression used, options are gzip or bzip2.
# bzip2 will produce slightly smaller files but is more processor intensive so
# may take longer to complete.
#
# LATEST is to store an additional copy of the latest backup to a standard
# location so it can be downloaded bt thrid party scripts.
#
# Use PREBACKUP and POSTBACKUP to specify Per and Post backup commands
# or scripts to perform tasks either before or after the backup process.
#
#
#=====================================================================
# Backup Rotation..
#=====================================================================
#
# Daily Backups are rotated weekly..
# Weekly Backups are run by default on Saturday Morning when
# cron.daily scripts are run...Can be changed with DOWEEKLY setting..
# Weekly Backups are rotated on a 5 week cycle..
# Monthly Backups are run on the 1st of the month..
# Monthly Backups are NOT rotated automatically...
# It may be a good idea to copy Monthly backups offline or to another
# server..
#
#=====================================================================
# Please Note!!
#=====================================================================
#
# I take no resposibility for any data loss or corruption when using
# this script..
# This script will not help in the event of a hard drive crash. If a 
# copy of the backup has not be stored offline or on another PC..
# You should copy your backups offline regularly for best protection.
#
# Happy backing up...
#
#=====================================================================
# Restoring
#=====================================================================
# Firstly you will need to uncompress the backup file.
# eg.
# gunzip file.gz (or bunzip2 file.bz2)
#
# Next you will need to use the psql client to restore the DB from the
# out file.
# eg.
# sudo -u postgres psql -d database -f /path/file.out
#
# As an option you also could use pg_restore.
#
# Lets hope you never have to use this.. :)
#
#=====================================================================
#=====================================================================
#=====================================================================
#
# Should not need to be modified from here down!!
#
#=====================================================================
#=====================================================================
#=====================================================================
PATH=/usr/local/bin:/usr/bin:/bin 
DATE=`date +%Y-%m-%d_%Hh%Mm`						# Datestamp e.g 2002-09-21
DOW=`date +%A`										# Day of the week e.g. Monday
DNOW=`date +%u`										# Day number of the week 1 to 7 where 1 represents Monday
DOM=`date +%d`										# Date of the Month e.g. 27
M=`date +%B`										# Month e.g January
W=`date +%V`										# Week Number e.g 37
VER=1.0												# Version Number
HOST=`hostname`										# Hostname
LOGFILE=$BACKUPDIR/$HOST-`date +%N`.log				# Logfile Name
LOGERR=$BACKUPDIR/ERRORS_$HOST-`date +%N`.log		# Error Logfile Name
BACKUPFILES=""
PARAM="" 											# Parameters passed to pg_dump

# Create required directories
if [ ! -e "$BACKUPDIR" ]			# Check Backup Directory exists.
	then
	mkdir -p "$BACKUPDIR"
fi

if [ ! -e "$BACKUPDIR/daily" ]		# Check Daily Directory exists.
	then
	mkdir -p "$BACKUPDIR/daily"
fi

if [ ! -e "$BACKUPDIR/weekly" ]		# Check Weekly Directory exists.
	then
	mkdir -p "$BACKUPDIR/weekly"
fi

if [ ! -e "$BACKUPDIR/monthly" ]	# Check Monthly Directory exists.
	then
	mkdir -p "$BACKUPDIR/monthly"
fi

if [ "$LATEST" = "yes" ]
then
	if [ ! -e "$BACKUPDIR/latest" ]	# Check Latest Directory exists.
	then
		mkdir -p "$BACKUPDIR/latest"
	fi
eval rm -fv "$BACKUPDIR/latest/*"
fi

# IO redirection for logging.
touch $LOGFILE
exec 6>&1           # Link file descriptor #6 with stdout.
                    # Saves stdout.
exec > $LOGFILE     # stdout replaced with file $LOGFILE.
touch $LOGERR
exec 7>&2           # Link file descriptor #7 with stderr.
                    # Saves stderr.
exec 2> $LOGERR     # stderr replaced with file $LOGERR.


# Functions

# Database dump function
dbdump () {
        #vacuumdb --all --analyze
        # little hack... vacuumdb writes to STDERR "vaccumdb: vacuuming database" for each DB it vaccums
        #                i don't want to have that on STDERR
        #                redirect STDOUT to file fstdout
        #                redirect STDERR to STDOUT
        #                grep STDOUT and filter 'vaccumdb: vacuuming database' away
        #                write the filtered output to fstderr
        #                cat fstdout to STDOUT
        #                cat fstderr to STDERR
        (vacuumdb --all --analyze 1>/tmp/fstdout ) 2>&1 | grep -v '^vacuumdb: vacuuming database' > /tmp/fstderr
        cat /tmp/fstdout
        cat /tmp/fstderr 1>&2
        rm /tmp/fstdout
        rm /tmp/fstderr

	pg_dump $PARAM $1 > $2		# Run pg_dump under the user 'postgres'
	return 0
}

# Compression function plus latest copy
SUFFIX=""
compression () {
	if [ "$COMP" = "gzip" ]; then
		gzip -f "$1"
		echo
		echo Backup Information for "$1"
		gzip -l "$1.gz"
		SUFFIX=".gz"
	elif [ "$COMP" = "bzip2" ]; then
		echo Compression information for "$1.bz2"
		bzip2 -f -v $1 2>&1
		SUFFIX=".bz2"
	else
		echo "No compression option set, check advanced settings"
	fi
	if [ "$LATEST" = "yes" ]; then
		cp $1$SUFFIX "$BACKUPDIR/latest/"
	fi	
	return 0
}

# Run command before we begin
if [ "$PREBACKUP" ]
	then
	echo ======================================================================
	echo "Prebackup command output."
	echo
	eval $PREBACKUP
	echo
	echo ======================================================================
	echo
fi

# Check if DROP DATABASE should be included in Dump
if [ "$DROP_DATABASE" = "yes" ]; then 
	PARAM="$PARAM --clean"
fi

# If backing up all DBs on the server
if [ "$DBNAMES" = "all" ]; then

	# Extract the names of all databases		
    DBNAMES="`psql -l |sed -n 4,/\eof/p | grep -v rows\) | awk {'print $1'}`"

	# If DBs are excluded
	for exclude in $DBEXCLUDE
	do
		DBNAMES=`echo $DBNAMES | sed "s/\b$exclude\b//g"`
	done

    MDBNAMES=$DBNAMES
fi
	
echo ======================================================================
echo AutoPostgreSQLBackup v$VER
echo -------------------------
echo 
echo Backup of Database Server - $HOST
echo Backup Start Time `date`
echo ======================================================================

# Test if seperate DB backups are required
if [ "$SEPDIR" = "yes" ]; then
	# Monthly Full Backup of all Databases
	if [ $DOM = "01" ]; then
		for MDB in $MDBNAMES
		do
			# Prepare $DB for using
			MDB="`echo $MDB | sed 's/%/ /g'`"		# Replace '%' with ' '

			if [ ! -e "$BACKUPDIR/monthly/$MDB" ]	# Check Monthly DB Directory exists.
			then
				mkdir -p "$BACKUPDIR/monthly/$MDB"
			fi
			echo Monthly Backup of $MDB...
				dbdump "$MDB" "$BACKUPDIR/monthly/$MDB/${MDB}_$DATE.$M.$MDB.out"
				compression "$BACKUPDIR/monthly/$MDB/${MDB}_$DATE.$M.$MDB.out"
				BACKUPFILES="$BACKUPFILES $BACKUPDIR/monthly/$MDB/${MDB}_$DATE.$M.$MDB.out$SUFFIX"
			echo ----------------------------------------------------------------------
		done
	fi

	for DB in $DBNAMES
	do
		# Prepare $DB for using
		DB="`echo $DB | sed 's/%/ /g'`" 			# Replace '%' with ' '

		# Weekly Backup
		if [ $DNOW = $DOWEEKLY ]; then
			# Create Seperate directory for each DB
			if [ ! -e "$BACKUPDIR/weekly/$DB" ]		# Check Weekly DB Directory exists.
				then
				mkdir -p "$BACKUPDIR/weekly/$DB"
			fi

			echo Weekly Backup of Database \( $DB \)
			echo Rotating 5 weeks Backups...
				if [ "$W" -le 05 ];then
					REMW=`expr 48 + $W`
				elif [ "$W" -lt 15 ];then
					REMW=`expr $W - 5`
				else
					REMW=`expr $W - 5`
				fi
			eval rm -fv "$BACKUPDIR/weekly/$DB_week.$REMW.*" 
			echo
				dbdump "$DB" "$BACKUPDIR/weekly/$DB/${DB}_week.$W.$DATE.out"
				compression "$BACKUPDIR/weekly/$DB/${DB}_week.$W.$DATE.out"
				BACKUPFILES="$BACKUPFILES $BACKUPDIR/weekly/$DB/${DB}_week.$W.$DATE.out$SUFFIX"
			echo ----------------------------------------------------------------------

		# Daily Backup
		else
			# Create Seperate directory for each DB
			if [ ! -e "$BACKUPDIR/daily/$DB" ]		# Check Daily DB Directory exists.
				then
				mkdir -p "$BACKUPDIR/daily/$DB"
			fi

			echo Daily Backup of Database \( $DB \)
			echo Rotating last weeks Backup...
			eval rm -fv "$BACKUPDIR/daily/$DB/*.$DOW.out.*" 
			echo
				dbdump "$DB" "$BACKUPDIR/daily/$DB/${DB}_$DATE.$DOW.out"
				compression "$BACKUPDIR/daily/$DB/${DB}_$DATE.$DOW.out"
				BACKUPFILES="$BACKUPFILES $BACKUPDIR/daily/$DB/${DB}_$DATE.$DOW.out$SUFFIX"
			echo ----------------------------------------------------------------------
		fi
	done

else # One backup file for all DBs
	# Monthly Full Backup of all Databases
	if [ $DOM = "01" ]; then
		echo Monthly full Backup of \( $MDBNAMES \)...
			dbdump "$MDBNAMES" "$BACKUPDIR/monthly/$DATE.$M.all-databases.out"
			compression "$BACKUPDIR/monthly/$DATE.$M.all-databases.out"
			BACKUPFILES="$BACKUPFILES $BACKUPDIR/monthly/$DATE.$M.all-databases.out$SUFFIX"
		echo ----------------------------------------------------------------------
	fi

	# Weekly Backup
	if [ $DNOW = $DOWEEKLY ]; then
		echo Weekly Backup of Databases \( $DBNAMES \)
		echo
		echo Rotating 5 weeks Backups...
			if [ "$W" -le 05 ];then
				REMW=`expr 48 + $W`
			elif [ "$W" -lt 15 ];then
				REMW=`expr $W - 5`
			else
				REMW=`expr $W - 5`
			fi
		eval rm -fv "$BACKUPDIR/weekly/week.$REMW.*" 
		echo
			dbdump "$DBNAMES" "$BACKUPDIR/weekly/week.$W.$DATE.out"
			compression "$BACKUPDIR/weekly/week.$W.$DATE.out"
			BACKUPFILES="$BACKUPFILES $BACKUPDIR/weekly/week.$W.$DATE.out$SUFFIX"
		echo ----------------------------------------------------------------------
	
	# Daily Backup
	else
		echo Daily Backup of Databases \( $DBNAMES \)
		echo
		echo Rotating last weeks Backup...
		eval rm -fv "$BACKUPDIR/daily/*.$DOW.out.*" 
		echo
			dbdump "$DBNAMES" "$BACKUPDIR/daily/$DATE.$DOW.out"
			compression "$BACKUPDIR/daily/$DATE.$DOW.out"
			BACKUPFILES="$BACKUPFILES $BACKUPDIR/daily/$DATE.$DOW.out$SUFFIX"
		echo ----------------------------------------------------------------------
	fi
fi

echo
echo Total disk space used for backup storage:
echo Size - Location
echo `du -hs "$BACKUPDIR"`
echo
echo Backup End `date`
echo ======================================================================

# Run command when we're done
if [ "$POSTBACKUP" ]
	then
	echo ======================================================================
	echo "Postbackup command output."
	echo
	eval $POSTBACKUP
	echo
	echo ======================================================================
fi

#Clean up IO redirection
exec 1>&6 6>&-      # Restore stdout and close file descriptor #6.
exec 1>&7 7>&-      # Restore stdout and close file descriptor #7.

if [ "$MAILCONTENT" = "files" ]
then
	if [ -s "$LOGERR" ]
	then
		# Include error log if is larger than zero.
		BACKUPFILES="$BACKUPFILES $LOGERR"
		ERRORNOTE="WARNING: Error Reported - "
	fi
	
	#Get backup size
	ATTSIZE=`du -c $BACKUPFILES | grep "[[:digit:][:space:]]total$" |sed s/\s*total//`
	if [ $MAXATTSIZE -ge $ATTSIZE ]
	then
		BACKUPFILES=`echo "$BACKUPFILES" | sed -e "s# # -a #g"`	#enable multiple attachments
		mutt -s "$ERRORNOTE PostgreSQL Backup Log and dumped Files for $HOST - $DATE" $BACKUPFILES $MAILADDR < $LOGFILE		#send via mutt
	else
		cat "$LOGFILE" | mail -s "WARNING! - PostgreSQL Backup exceeds set maximum attachment size on $HOST - $DATE" $MAILADDR
	fi
elif [ "$MAILCONTENT" = "log" ]
then
	cat "$LOGFILE" | mail -s "PostgreSQL Backup Log for $HOST - $DATE" $MAILADDR
	if [ -s "$LOGERR" ]
		then
			cat "$LOGERR" | mail -s "ERRORS REPORTED: PostgreSQL Backup error Log for $HOST - $DATE" $MAILADDR
	fi	
elif [ "$MAILCONTENT" = "quiet" ]
then
	if [ -s "$LOGERR" ]
		then
			cat "$LOGERR" | mail -s "ERRORS REPORTED: PostgreSQL Backup error Log for $HOST - $DATE" $MAILADDR
			cat "$LOGFILE" | mail -s "PostgreSQL Backup Log for $HOST - $DATE" $MAILADDR
	fi
else
	if [ -s "$LOGERR" ]
		then
			cat "$LOGFILE"
			echo
			echo "###### WARNING ######"
			echo "Errors reported during AutoPostgreSQLBackup execution.. Backup failed"
			echo "Error log below.."
			cat "$LOGERR"
	else
		cat "$LOGFILE"
	fi	
fi

if [ -s "$LOGERR" ]
	then
		STATUS=1
	else
		STATUS=0
fi

# Clean up Logfile
eval rm -f "$LOGFILE"
eval rm -f "$LOGERR"

exit $STATUS
