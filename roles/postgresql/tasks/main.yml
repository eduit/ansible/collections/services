---
- name: Check that the pg binaries are accessible from the shell
  ansible.builtin.copy:
    dest: /etc/profile.d/postgres_binary.sh
    content: "export PATH=$PATH:/usr/pgsql-{{ postgresql_version }}/bin/"
    owner: root
    group: root
    mode: "0755"
  when: postgresql_version is regex("^[0-9]+$") and postgresql_use_official_repo

- name: Include OS specific installation tasks
  ansible.builtin.include_tasks: "setup_{{ ansible_os_family | lower }}.yml"

- name: Apply OS tuning for SQL servers
  ansible.builtin.include_role:
    name: let.base_settings.os_tuning
    tasks_from: sql.yml

- name: Change service name for upstream package
  ansible.builtin.set_fact:
    postgresql_service: "postgresql-{{ postgresql_version }}"
  when: postgresql_version is regex("^[0-9]+$") and postgresql_use_official_repo

# Prepare EduIT standard data directory
- name: Include task db_directory.yml
  ansible.builtin.include_tasks: db_directory.yml

- name: "Create required directories in {{ postgresql_data_dir }}"
  ansible.builtin.file:
    path: "/{{ postgresql_data_dir }}/{{ item }}"
    state: directory
    owner: postgres
    group: postgres
    mode: '0700'
  loop:
    - data
    - scripts
    - backups

- name: Check if the pg_hba.conf file exists and register it as a variable
  ansible.builtin.stat:
    path: "{{ postgresql_pg_hba }}"
  register: pg_hba_conf

- name: Set pg_hba.conf path as fact (for Zabbix client installation)
  ansible.builtin.set_fact:
    postgresql_pg_hba: "{{ postgresql_pg_hba }}"

- name: Overwrite DB init script name for upstream package
  ansible.builtin.set_fact:
    postgresql_setup_script: "/usr/bin/postgresql-{{ postgresql_version }}-setup"
  when: postgresql_version is regex("^[0-9]+$") and postgresql_use_official_repo

- name: Execute postgreql setup if its a new installation
  ansible.builtin.command: "{{ postgresql_setup_script }} initdb"
  notify: Restart PostgreSQL
  changed_when: true
  when: not pg_hba_conf.stat.exists

- name: Include tasks for server configuration
  ansible.builtin.include_tasks: db_configuration.yml

- name: Restart service if required
  ansible.builtin.meta: flush_handlers

# Populate DB with users and schemas
- name: Include task db_contents.yml
  ansible.builtin.include_tasks: db_contents.yml

# Configure backup
- name: Include task backup.yml
  ansible.builtin.include_tasks: backup.yml

- name: Zabbix registration for PostgreSQL
  when: >
    zabbix_client_host_metadata is defined
    and inventory_hostname_short in groups.Production
    or (groups.zabbix_clients is defined and inventory_hostname_short in groups.zabbix_clients)
  block:
    - name: Extend Zabbix agent registration string
      ansible.builtin.set_fact:
        zabbix_client_host_metadata: "{{ zabbix_client_host_metadata }} postgresql"
    - name: Configure PostgreSQL monitoring
      ansible.builtin.include_role:
        name: let.base_settings.zabbix_client
        tasks_from: appconfigs/postgres.yml

# Initiate restore if requested
- name: Initial database restore
  ansible.builtin.include_tasks:
    file: restore.yml
  when: postgresql_restore_full_filepath | length > 0

- name: Enable PostgreSQL service on reboot
  ansible.builtin.systemd_service:
    name: "{{ postgresql_service }}"
    enabled: "{{ postgresql_enabled_on_startup }}"
