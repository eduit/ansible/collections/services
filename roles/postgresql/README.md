# Configure PostgreSQL

In *SnipeIT*, set attribute *Ansible: postgres* to activate!

## Minimal Configuration
### Package and Version

Without additional settings, the default OS package is used. To use the upstream repo, set
```
postgresql_use_official_repo: true
```

The version is selected by
```
postgresql_version: "16"
```

If only the version is set, an OS app stream module is used (Redhat).

### Network Connectivity
To overwrite the default *localhost* listening address, set

```
postgresql_bind_address: "*"
```

For a local firewall, a shortcut for clients on Kubernetes is available:
```
db_from_k8s: true
```
opens the firewall for the k8s nodes. 

### Fine Tuning
Options in `postgresql.conf` may be added to a list, and will be written to `conf.d/10-ansible_parameters.conf`:
```
postgresql_parameters:
  - effective_cache_size = 15GB
  - maintenance_work_mem = 1280MB
  - checkpoint_completion_target = 0.7
```

### Backup
Activate DB backups with
```
postgresql_backup_db: true
```
A backup script and cron task will be deployed. It backups to
```
postgresql_backup_db_dir: "/mnt/backup/{{ inventory_hostname_short }}"
```
which is a NFS share. The default backup directory is set in `group_vars/all.yml`.

## Server Specific Configuration
Typical per server settings (in `host_vars/<servername>.yml` are

- databases
- users
- passwords

See [https://gitlab.ethz.ch/eduit/ansible/playbooks/linux-tasks/-/blob/master/host_vars/dp-portal-db-dev.yml?ref_type=heads](DP portal dev server configuration) for an example.

## Recommended Settings
The role has been developed with SELinux activated. The standard LET data directory is supported. Consider enabling SELinux, and also IPv6, wich are disabled in base_settings by default:

```
selinux_disable: false
ipv6_disable: false
```

