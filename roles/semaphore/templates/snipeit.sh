#!/usr/bin/bash
#
# Read and dump SnipeIT content as an Ansible inventory
export SNIPEIT_API_KEY="{{ semaphore_snipeit_apikey }}"
# Python requests should use system certs
export REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-bundle.crt

ansible-inventory -i /usr/local/share/inventory.yml --list
