- name: Check system disk usage on each host
  hosts: all
#  become: true
  gather_facts: false
  vars:
    max_log_file_size: 100  # in MB; threshold for large log files
    size_warning_threshold: 4  # in GB; warning threshold for yum/dnf cache and mail spool
    red_color_code: "\033[0;31m"  # ANSI code for red text
    reset_color_code: "\033[0m"   # ANSI code to reset color
    summaries_file: "/tmp/host_system_disk_usage_summary.txt"
  tasks:
    - name: Check if /var/cache/yum exists
      stat:
        path: /var/cache/yum
      register: yum_cache_dir

    - name: Check if /var/cache/dnf exists
      stat:
        path: /var/cache/dnf
      register: dnf_cache_dir

    - name: Get yum cache size in KB
      command: "du -s /var/cache/yum"
      register: yum_cache_size_kb
      when: yum_cache_dir.stat.exists
      changed_when: false
      ignore_errors: true

    - name: Get dnf cache size in KB
      command: "du -s /var/cache/dnf"
      register: dnf_cache_size_kb
      when: dnf_cache_dir.stat.exists
      changed_when: false
      ignore_errors: true

    - name: Set cache size based on available directory
      set_fact:
        cache_size_kb: "{{ (yum_cache_size_kb.stdout.split()[0] if yum_cache_dir.stat.exists else dnf_cache_size_kb.stdout.split()[0]) | default('0', true) }}"

    - name: Convert cache size to GB
      set_fact:
        cache_size: "{{ (cache_size_kb | int / 1024 / 1024) | round(2) }}"

    - name: Format cache size display with color if above threshold
      set_fact:
        cache_display: "{{ cache_size }} GB"
      when: cache_size | float <= size_warning_threshold
      failed_when: false

    - name: Apply red color if cache size exceeds threshold
      set_fact:
        cache_display: "{{ red_color_code + cache_size + ' GB' + reset_color_code }}"
      when: cache_size | float > size_warning_threshold
      failed_when: false

    - name: Get mail spool size in KB
      command: "du -s /var/spool/mail"
      register: mail_spool_size_kb
      changed_when: false

    - name: Convert mail spool size to GB
      set_fact:
        mail_spool_size: "{{ (mail_spool_size_kb.stdout.split()[0] | int / 1024 / 1024) | round(2) }}"

    - name: Format mail spool size display with color if above threshold
      set_fact:
        mail_spool_display: "{{ mail_spool_size }} GB"
      when: mail_spool_size | float <= size_warning_threshold
      failed_when: false

    - name: Apply red color if mail spool size exceeds threshold
      set_fact:
        mail_spool_display: "{{ red_color_code + mail_spool_size + ' GB' + reset_color_code }}"
      when: mail_spool_size | float > size_warning_threshold
      failed_when: false

    - name: Find large log files
      find:
        paths: /var/log
        recurse: yes
        size: "{{ max_log_file_size }}M"
      register: large_log_files
      changed_when: false

    - name: Sort and get top 5 largest log files
      set_fact:
        top_large_logs: "{{ large_log_files.files | sort(attribute='size', reverse=true) | map(attribute='path') | list | slice(5) }}"
   
    - name: Initialize host summary lines
      set_fact:
        host_summary_lines:
          - "Host: {{ inventory_hostname }}"
          - "Cache Size: {{ cache_display }}"
          - "Mail Spool Size: {{ mail_spool_display }}"
          - "Large Log Files:"
      when: top_large_logs is defined and top_large_logs

#    - name: Add large log files to host summary
#      set_fact:
#        host_summary_lines: "{{ host_summary_lines + ['- ' + (file|default('')) + ' (' + (large_log_files.files | selectattr('path', '==', file) | map('attribute', 'size_human') | first) + ')'] }}"
#      when: large_log_files.files|length > 0

    - name: Add large log files to host summary
      set_fact:
        host_summary_lines: "{{ host_summary_lines + ['- ' + file + ' (' + (large_log_files.files | selectattr('path', '==', file) | map('attribute', 'size_human') | first) + ')'] }}"
      when: large_log_files.files|length > 0 and file is defined

    - name: Add 'No large log files found' to host summary
      set_fact:
        host_summary_lines: "{{ host_summary_lines + ['- No large log files found.'] }}"
      when: top_large_logs is not defined or top_large_logs | length == 0
 
    - name: Combine host summary lines
      set_fact:
        host_summary: "{{ host_summary_lines | join('\n') }}"

    - name: Append host summary to global summary
      set_fact:
        summary_list: "{{ summary_list | default([]) + [host_summary] }}"
      delegate_to: localhost
    
    - name: Display summary for all hosts
      debug:
        msg: "{{ summary_list }}"
      run_once: true
   
    - name: Combine host summary lines
      set_fact:
        host_summary: "{{ host_summary_lines | join('\n') }}"

    - name: Append host summary to global summary
      set_fact:
        summary_list: "{{ summary_list | default([]) + [host_summary] }}"
      delegate_to: localhost
   
