# Ansible Semaphore Server

Deploy a Semaphore UI Server.

## Configuration
Host/group vars can be found in `defaults/main.yml`.

Potential group vars to be overwritten are:

- `mariadb_backup_db`: service uses MariaDB, DB backup is recommended
- `mariadb_backup_db_name`: `['semaphore']` only DB present
- `semaphore_db_password`: to connect to local DB server
- `semaphore_ldap_bindpassword`: to enable LDAP
- `semaphore_ansible_vault_password`: to access encrypted stuff
- `semaphore_snipeit_apikey`: to access SnipeIT as Ansible repository
- `semaphore_cookie_hash`: Semaphore internal
- `semaphore_cookie_encryption`: Semaphore internal
- `semaphore_access_key_encryption`: Semaphore internal

All semaphore variables must be encrypted by `ansible-vault`. Semaphore internals should be reused if migrating to a new server.

Host vars to be set:

- `mariadb_databases`, `mariadb_users`: see below
- `acmedns_username`, `acmedns_password`, `acmedns_subdomain`: from Netcenter when DNS challenge has been enabled
- `acme_sh_subject_names`: `[semaphore.ethz.ch]` for acme.sh certificate challenge
- `semaphore_hostname`: `semaphore.ethz.ch` for Semaphore site configuration

Database configuration example:

```yaml
mariadb_databases:
  - name: semaphore
    collation: utf8_bin
    encoding: utf8
    replicate: false
mariadb_users:
  - name: semaphore
    host: 127.0.0.1
    password: "{{ semaphore_db_password }}"
    priv: "semaphore.*:ALL"
    state: present
```
