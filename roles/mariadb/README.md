# MariaDB Setup

In *SnipeIT*, set attribute *Ansible: mariadb* to activate!

## Requirements
Requires *fauust.mariadb* (https://github.com/fauust/ansible-role-mariadb). Because this is only a role, ansible-galaxy is unable to install it as a requirement.

## Minimal Configuration
### Package and Version
Without additional settings, the default OS package is used. To use the upstream repo, set
```
mariadb_use_official_repo: true
```

The version is selected by
```
mariadb_use_official_repo_version: "10.10"
```
### Network Connectivity
To overwrite the default *localhost* listening address, set

```
mariadb_bind_address: 0.0.0.0
```

For a local firewall, a shortcut for clients on Kubernetes is available:
```
db_from_k8s: true
```
opens the firewall for the k8s nodes. 

### Backup
Activate DB backups with
```
mariadb_backup_db: true
```
A backup script and cron task will be deployed. It backups to
```
mariadb_backup_db_dir: "/mnt/backup/{{ inventory_hostname_short }}"
```
which is a NFS share. The default backup directory is set in `group_vars/all.yml`.

Mandatory list of databases to be saved:
```
mariadb_backup_db_name: []
```

## Server Specific Configuration
Typical per server settings (in `host_vars/<servername>.yml` are

- databases
- users
- passwords

See [https://gitlab.ethz.ch/eduit/ansible/playbooks/linux-tasks/-/blob/master/host_vars/sebserver-db-dev.yml?ref_type=heads](SEB-Server dev server configuration) for an example.

## Recommended Settings
The role has been developed with SELinux activated. The standard LET data directory is supported. Consider enabling SELinux, and also IPv6, wich are disabled in base_settings by default:

```
selinux_disable: false
ipv6_disable: false
```
