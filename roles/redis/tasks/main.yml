---
- name: Install redis package
  ansible.builtin.package:
    name: redis
    state: present

- name: Ensure data directory exists
  ansible.builtin.file:
    path: "{{ redis_data_path }}"
    state: directory
    owner: redis
    group: redis
    mode: "0755"

- name: Ensure configuration directory for includes exists
  ansible.builtin.file:
    path: /etc/redis/redis.conf.d
    state: directory
    owner: root
    group: root
    mode: "0755"

- name: Deploy basic configuration include
  ansible.builtin.template:
    src: basic.conf
    dest: /etc/redis/redis.conf.d/10-basic.conf
    owner: root
    group: root
    mode: "0644"
  notify: Restart redis

# Redis 6.x does not support includes with patterns, only 7.x and later
- name: Add include directory to redis configuration
  ansible.builtin.lineinfile:
    path: /etc/redis/redis.conf
    line: include /etc/redis/redis.conf.d/10-basic.conf
    insertafter: EOL

- name: Open external access to redis
  when: not redis_local_only
  block:
    - name: Deploy configuration opening redis for other hosts
      ansible.builtin.copy:
        src: external_access.conf
        dest: /etc/redis/redis.conf.d/20-external_access.conf
        owner: root
        group: root
        mode: "0644"
      notify: Restart redis

    - name: "Adding Firewall rule service redis for remote redis access"
      ansible.posix.firewalld:
        service: "redis"
        permanent: true
        state: enabled

    - name: Add include directory to redis configuration
      ansible.builtin.lineinfile:
        path: /etc/redis/redis.conf
        line: include /etc/redis/redis.conf.d/20-external_access.conf
        insertafter: EOL

- name: Extend Zabbix agent registration string
  ansible.builtin.set_fact:
    zabbix_client_host_metadata: "{{ zabbix_client_host_metadata }} redis"
  when: >
    zabbix_client_host_metadata is defined
    and inventory_hostname_short in groups.Production
    or (groups.zabbix_clients is defined and inventory_hostname_short in groups.zabbix_clients)

- name: Ensure redis service is enabled
  ansible.builtin.service:
    name: redis
    enabled: true
    state: started
