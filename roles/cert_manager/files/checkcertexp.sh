#!/bin/bash +x
# runs through all folders, checks for cert files and if it finds one prints its expiry date
oldpwd=$(pwd)
cd /data_1/zertifikate

for f in */
do
cd $f
echo $f
if [ -f *.crt ]; then
	openssl x509 -noout -in *.crt -enddate
fi
echo "-------------------------------------"
cd ..
done

cd $oldpwd
