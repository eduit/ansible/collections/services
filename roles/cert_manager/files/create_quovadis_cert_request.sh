#!/bin/bash

#####
#
# Create QuoVadis Certificate Request for Domain 'ethz.ch'.
#
# Usage:
#   create_quovadis_cert_request.sh --> interactive mode
#   create_quovadis_cert_request.sh -c <cn> [-s <san>...-s <san>]
#   create_quovadis_cert_request.sh -h | --help
#   create_quovadis_cert_request.sh -v| --version
#
# Options:
#   none          Invokes interactive mode.
#   -c <cn>       CN (Common Name) of certifcate.
#   -s <san>      SAN(s) (Subject Alternative Name(s)) of certifcate (up to 9 SANs may by provided).
#   -h --help     Show this screen.
#   -v --version  Show version.
#
# Testcases:
# create_quovadis_cert_request.sh -c -s FQDN --> OK
# create_quovadis_cert_request.sh -c FQDN1 -s -s FQDN2 --> OK
# create_quovadis_cert_request.sh -c -h --> OK
# create_quovadis_cert_request.sh -c FQDN1 -c FQDN2 -h --> OK
# create_quovadis_cert_request.sh -c FQDN -s --> OK (handled by getopts)
#
# ToDo:
#   - show request or cert as text output (openssl req -text -in <request>, openssl x509 -text -in <cert>)
#   - verify wether a cert and a key belong together (compare both moduli)
#   - handle the case when CN is not the first argument --> done
#   - allow to exit interactive mode --> type 'q' when asking for CN --> done
#
# Vers. 1.00 : bm-20200708 - initial release
#       1.10 : bm-20200731 - changes:
#                          - output text is partly no longer handeld by functions (it's easier to format multiline output text when stored in variables)
#       1.11 : bm-20200824 - fixed a glitch in path construction to cert directories
#       1.12 : bm-20200824 - fixed a typo which was preventing to script from runnning successfully
#       1.13 : bm-20210105 - list of authorized persons updated
#       1.20 : bm-20210302 - maximal number SANs changed to 9 because of changes in the certificate request process of ETHZ
#                          - helptext and hints adopted to the new request process of ETHZ
#       1.21 : bm.20210721 - hint to show help added
#       1.22 : bg-20240228 - Mail address EduIT; cert store server name 'flanders'
#
version="1.22"
#
#####

#
#set -x
#

#
### Variables
#
scpUser="root"
certStoreHost="certmanager.ethz.ch"
pathToCertsStore="/data_1/zertifikate"
outdatedCertsDir="old_certs"
# Write log entries (path must exist; disabled when set to: logFile="")
logFile="$pathToCertsStore/request_logs/create_request_log"
#
## All the variables below usually don't need any adaptions
#
maxSans=${maxSans:-"9"} # default value for ETHZ requests (max = 9)
certCn=""
certSan=()
## The cert stuff
openSslConf="/etc/pki/tls/openssl.cnf" # this is only needed to load the ssl v3 extension properly
# [ req ]
defaultBits="2048"
defaultMd="sha256"
# [ dn ]
country="CH"
state="Zuerich"
location="Zuerich"
organisation="ETH Zuerich"
emailAddress="eduit-sysadmins@id.ethz.ch"
#

helpText="
Create QuoVadis Certificate Request for Domain 'ethz.ch'.: vers. $version

Usage:
  ${0##*/}
  ${0##*/} -c <cn> [-s <san>...-s <san>]
  ${0##*/} -h | --help
  ${0##*/} -v | --version

Options:
                Invokes interactive mode.
  -c <cn>       CN (Common Name) of certifcate.
  -s <san>      SAN(s) (Subject Alternative Name(s)) of certifcate (up to 9 SANs may by provided).
  -h --help     Show this screen.
  -v --version  Show version.
 

This script creates a certificate request for the domain 'ethz.ch' containing either a single CN (Common Name)
or one CN and up to 9 SANs (Subject Alternative Names). With this request you are able to issue the corres-
ponding server certifcate either based on the QuoVadis Root CA or the ETH Root Certificate in the PKI frontend
of ETHZ (https://pki-frontend.ethz.ch). The CN and all SANs must already have an existing DNS entry in
'https://www.netcenter.ethz.ch/ip' to successfully create the cerificate request as all CNs will be validated
before the certificate request will be created.

The script can be used in the following two ways:
- interactive mode: just execute the script without any arguemnts, you will then be prompted
  for the mandatory CN and the optional SANs
- automatic mode: execute the script with the option '-c' followed by the CN and up to 9
  times the option '-s' each followed by a CN

The script also handles the storage of the certificate files. Therefor a directory with the CN of the
certificate as its name will be created in the directory where all certificates of LET are stored. If
this directory already exists the script will use this one. All certicate files already existing in the
certificate store will be moved to a subdirectory named 'old_certs' before the new cerificate request
will be saved.
Don't forget to copy the issued SSL server certificate to the corresponding directory in the LET
certicate store for further usage.
"


#
### Functions
#
helptext ()
{
    echo "$helpText"
    if [ -n "$logFile" ] ; then
        echo "All certificate requests will be logged in '$logFile'."
    else
        echo "Certificate requests are not logged! If you want to do so, you can set the path to a log file"
        echo "in the variable 'logFile'."
    fi
    printf "\n"
    exit 0
}
#
## Show hint depending on which arguments have been provided incorrectly
#
usage ()
{
    printf "\n"
    echo "------"
    printf "\n"
    if [ "$1" == "cnnotok" ]; then
        echo "You either provided no or more than one CN or you invoked the script with an invalid option."
    elif [ "$1" == "flagfollows" ]; then
        echo "At least one -c or -s flag has no corresponding FQDN."
    elif [ "$1" == "notavalidfqdn" ]; then
        echo "Any CN or SAN provided is not a valid FQDN."
    elif [ "$1" == "missingfqdn" ]; then
        echo "Either CN or SAN is missing."
    elif [ "$1" == "invalidoption" ]; then
        echo "Invalid option provided."
    elif [ "$1" == "cnorsannotunique" ]; then
        echo "CN or a SAN can only be provided once!."
    elif [ "$1" == "tomanysans" ]; then
        echo "More than the maximal allowed number of 9 SANs provided."
    fi
    printf "\n"
    echo "------"
    helptext
}
#
## Tests whether the the CN or SAN is a FQDN and registered in DNS of ETHZ
##  Handles also the case when an empty string is handed over to the function.
#
validFqdn ()
{
    local __fqdn=$1
    if [[ "$__fqdn" ]]; then
        if [ "$(dig +short "$__fqdn" &2>/dev/null)" ]; then
            echo "True"
        else
            echo "False"
        fi
    else
        echo "False"
    fi
}
#
readInput ()
{
    ## Read CN
    local __finished="False"
    printf "\n"
    while [ "$__finished" != "True" ]; do
        read -p "Common Name (CN) of certificate followed by <ENTER>: " certCn
        if [[ "$certCn" == "q" ||  "$certCn" == "quit" ]]; then
            printf "\n"
            echo "Exit the script"
            printf "\n"
            exit 0
        fi
        if [[ "$certCn" ]]; then
            if [[ "$(validFqdn "$certCn")" == "True" ]]; then
                __finished="True"
             else
                 printf "\n"
                echo "$certCn is not a valid FQDN. Check spelling of CN!"
                printf "\n"
            fi
        else
            printf "\n"
            echo "Please provide a CN or type 'q' followed by <ENTER> to quit..."
            printf "\n"
        fi
    done
    #
    ## Read SAN(s)
    #
    local __newSan=""
    local __finished="False"
    local __i=0
    printf "\n"
    while [ "$__finished" != "True" ]; do
        printf "\n"
        echo "Please provide a SAN or hit just <ENTER> to leave this section..."
        printf "\n"
        read -p "Subject Alternative Name (SAN) $(($__i+1)) of certificate followed by <ENTER>: " __newSan
           if [ -z "$__newSan" ]; then
              __finished="True"
         else
               # Test for valid FQDN: if yes --> add SAN to array
            if [[ "$(validFqdn "$__newSan")" == "True" ]]; then
                certSan+=(${__newSan})
                __i=$((__i+1))
                if [ $__i -gt $maxSans ]; then
                    usage tomanysans
                fi
            else
                printf "\n"
                echo "$__newSan ist not a valid FQDN! Check spelling of SAN!"
            fi
        fi
    done
}
#

#
### Main
#

#
## Read and test the arguments
# - -h flag provided --> helptext
# - more than one -c flag provided --> usage
# - flagfollows --> usage
# - missing FQDN after -c or -s flag is handled by testing for valid FQDN
# - no arguments provided --> interactive mode
# - else --> assign arguments to variable
#
if [ $# -ne 0 ]; then # we invoked the script with arguments --> we evaluate the arguments
    if [[ " $@ " =~ " -h " || " $@ " =~ " --help " ]]; then
        helptext
    elif [[ " $@ " =~ " -v " || " $@ " =~ " --version " ]]; then
        printf "\n"
        echo "${0##*/}: vers. $version"
        printf "\n"
        exit 0
    fi
    #
    ## Test whether any '-c' or '-s' flag followed by another flag to omit printing 'usage' of 'dig'
    ## The case when only a -c or a -s flag is provided (missing the mandatory FQDN) is handled when testing for valid FQDN!
    k=0
    for arg in "$@"; do
        if [ "$arg" == "-c" ] || [ "$arg" == "-s" ]; then
            m=$((k+2)) # as position of '$arg' = '$X' + 1
            # Print usage if '${!m}' is a flag (begins with '-')
            if [[ "${!m}" == -* ]]; then
                usage flagfollows
            fi
        fi
        k=$((k+1))
    done
    ## Test whether exactly one CN is provided
    i=0
    for arg in "$@"; do
        if [ "$arg" == "-c" ]; then
            i=$((i+1))
        fi
    done
    if [ $i -ne 1 ]; then
        usage cnnotok
    fi
    ### Parse the arguments (we don't have to parse 'help' and 'version' here as these have already been handled above)
    #
    while getopts ":s:c:" opt; do
        case $opt in
            s)  set -f # disable glob
                certSan+=($OPTARG) ;; # use the split+glob operator
            c)  certCn=$OPTARG ;;
            :)  usage missingfqdn 1>&2 ;; # handling missing argument
            \? ) usage invalidoption 1>&2    # handling invalid option
        esac
    done
    shift $((OPTIND -1))
    if [[ "${#certSan[@]}" -gt "$maxSans" ]]; then
        usage tomanysans
    fi
    # Test CN and SAN(s) whether valid FQDN
    if [[ "$(validFqdn "$certCn")" == "False" ]]; then
        usage notavalidfqdn
    fi
    if [ "$certSan" ]; then
        for element in "${certSan[@]}"; do
            if [[ "$(validFqdn "$element")" == "False" ]]; then
                usage notavalidfqdn
            fi
        done
    fi
else # no arguments provided when executing the script --> we are in interactive mode
    printf "\n"
    echo "Interactive mode - please note: Only one CN and up to 9 SANs may be provided."
    echo "                                The CN is mandatory, SANs are optional."
    printf "\n"
    echo "Please provide a CN or type 'q' followed by <ENTER> to quit..."
    printf "\n"
    echo "Show help: exit the script and invoke help: create_quovadis_cert_request.sh -h"
    readInput
fi
#

#
## Test whether every FQDN occurs only once
##  First we compare CN and SAN(s)
#
if [[ -n "$certSan" ]]; then
    for element in "${certSan[@]}"; do
        if [ "${element}" = "${certCn}" ]; then
               usage cnorsannotunique
        fi
    done
#
## Then we compare all SANs to each other, when more than one SAN has been provided
## Therefore we clone $certSan into a temporary array
#
    if [[ ${#certSan[@]} -gt 1 ]]; then
        i=0
        for element1 in "${certSan[@]}"; do
            certSanTemp=("${certSan[@]}")
            tempElement=${certSanTemp[$i]}
            unset 'certSanTemp[$i]'
            for element2 in "${certSanTemp[@]}"; do
                if [ "${element2}" = "${tempElement}" ]; then
                       usage cnorsannotunique
                   fi
            done
            i=$((i+1))
        done
        unset certSanTemp
    fi
fi
#

#
## Evaluate the input
#
printf "\n"
if [[ -z "$certSan" ]]; then
    echo "You provided the following CN: $certCn"
else 
    echo "You provided the following CN and SAN(s):"
    echo "CN:     $certCn"
    echo "SAN(s): ${certSan[@]}"
fi
printf "\n"
echo "If any of the FQDNs listed above is incorrect end the script and execute it again."
printf "\n"
read -r -p "Would you like to proceed [Y/n] " response
response=${response,,} # tolower
if [[ ! $response =~ ^(yes|y| ) ]] && [[ ! -z $response ]]; then
    printf "\n"
    echo "Exit the script"
    printf "\n"
    exit 0
fi
#
printf "\n"
echo "Creating the request..."
#

#
### Handling of necessary directories and possibly already existing files
#
## Create necessary directory if necessary
if [ ! -d "$pathToCertsStore/$certCn" ] ; then
    mkdir $pathToCertsStore/$certCn
fi
## Change ownership and permissions of this directory (already existing or just created)
chgrp root $pathToCertsStore/$certCn
chmod 700 $pathToCertsStore/$certCn
#
## Create directory for old (oudated) certificates if necessary
if [ ! -d "$pathToCertsStore/$certCn/$outdatedCertsDir" ] ; then
    mkdir $pathToCertsStore/$certCn/$outdatedCertsDir
fi
## Change ownership and permissions of this directory (already existing or just created)
chgrp root $pathToCertsStore/$certCn/$outdatedCertsDir
chmod 700 $pathToCertsStore/$certCn/$outdatedCertsDir
#
## Move existing files if necessary
startOfFileName=$( echo $certCn | cut -f 1 -d '.' )
ls -1 $pathToCertsStore/$certCn/$startOfFileName* > /dev/null 2>&1
if [ "$?" = "0" ]; then
    mv -f $pathToCertsStore/$certCn/$startOfFileName* $pathToCertsStore/$certCn/$outdatedCertsDir/
fi
#

#
### Create the certificate request
#
# Create string for SANs
# scheme for variable : 'DNS:san{1}[,DNS:san{2}]...[,DNS:san{n}]
if [[ ${certSan[@]} ]]; then
    certSanString=""
    for san in "${certSan[@]}"; do
        certSanString="${certSanString}DNS:${san},"
    done
    # remove the final ',' from the string
    certSanString=${certSanString::-1}
    # Create certificate request with SAN(s)
    openssl req \
            -new -nodes \
            -newkey rsa:$defaultBits \
            -$defaultMd \
            -keyout $pathToCertsStore/$certCn/$certCn.key \
            -out $pathToCertsStore/$certCn/$certCn.pem \
            -extensions v3_req \
            -subj "/C=$country/ST=$state/L=$location/O=$organisation/CN=$certCn/emailAddress=$emailAddress" \
            -reqexts SAN -config <(cat $openSslConf \
            <(printf "[SAN]\nsubjectAltName=$certSanString")) \
            > /dev/null 2>&1
else
    # Create certificate request for CN only
    openssl req \
            -new -nodes \
            -newkey rsa:$defaultBits \
            -$defaultMd \
            -keyout $pathToCertsStore/$certCn/$certCn.key \
            -out $pathToCertsStore/$certCn/$certCn.pem \
            -subj "/C=$country/ST=$state/L=$location/O=$organisation/CN=$certCn/emailAddress=$emailAddress" \
            > /dev/null 2>&1
fi
#
## Change permissions of the just created files
fileName="$pathToCertsStore/$certCn/$certCn.*"
find "$(dirname "$fileName")" -maxdepth 1 -name "$(basename "$fileName")" 2>/dev/null | while read file
do
    chmod 600 "$file"
done
#

#
### Create log file entry, if desired
#
if [[ -n "$logFile" ]]; then
    echo "$( date ) - certificate request created for '$certCn'" >> $logFile
    spacer="           "
    if [[ ${certSan[@]} ]]; then
        echo "${spacer}and the following SAN(s): ${certSanString}" >> $logFile
    fi
fi
#

#
## Print out the CN and the SANs of the certificate request.
#
crCn=$(openssl req -in $pathToCertsStore/$certCn/$certCn.pem -noout -text | grep CN | cut -d',' -f5 | cut -d '/' -f1)
crSan=$(openssl req -in $pathToCertsStore/$certCn/$certCn.pem -noout -text | grep DNS)
printf "\n"
echo "Certificate request successfully created."
echo $crCn
if [[ -n "$crSan" ]]; then
    echo $crSan
fi
#

#
## Display how to proceed with the request
#
printf "\n"
echo "If this is NOT correct execute the script again."
echo "Otherwise you can download the request file ($certCn.pem) with the following command (Unix/Linux):"
printf "\n"
echo "scp $scpUser@$certStoreHost:$pathToCertsStore/$certCn/$certCn.pem <local_path>"
printf "\n"
echo "Then request the certificate in the PKI frontend of ETHZ (https://pki-frontend.ethz.ch)."
printf "\n"
echo "And don't forget to copy the issued SSL server certificate to the corresponding directory in the"
echo "LET certificate store."
printf "\n"
#

exit 0

